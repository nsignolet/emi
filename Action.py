import appscript
import webbrowser

import Environment


def open_in_browser(id_environment):
    environment = Environment.get(id_environment)
    url = environment['url']

    webbrowser.open_new('http://' + url)


def open_admin_in_browser(id_environment):
    environment = Environment.get(id_environment)
    url = environment['admin_url']

    webbrowser.open_new('http://' + url)


def ssh(id_environment):
    environment = Environment.get(id_environment)
    ssh_user = environment['ssh_user']
    ssh_host = environment['url']
    ssh_port = environment['ssh_port']

    appscript.app('Terminal').do_script('ssh ' + ssh_user + '@' + ssh_host + ' -p ' + str(ssh_port))
