import sqlite3

with sqlite3.connect('data.db') as db:
    db.row_factory = sqlite3.Row
    cursor = db.cursor()


def get_fields():
    return [
        {'input': 'name', 'name': "Nom de l'environnement", 'default': ""},
        {'input': 'url', 'name': "URL", 'default': ""},
        {'input': 'admin_url', 'name': "URL Admin", 'default': ""},
        {'input': 'ssh_user', 'name': "Utilisateur SSH", 'default': ""},
        {'input': 'ssh_pass', 'name': "Mot de passe SSH", 'default': ""},
        {'input': 'ssh_port', 'name': "Port SSH", 'default': 22},
        {'input': 'db_name', 'name': "Base de données", 'default': ""},
        {'input': 'db_user', 'name': "Utilisateur DB", 'default': ""},
        {'input': 'db_pass', 'name': "Mot de passe DB", 'default': ""},
        {'input': 'db_url', 'name': "URL DB", 'default': ""},
        {'input': 'db_port', 'name': "Port DB", 'default': 3306},
        {'input': 'folder', 'name': "Dossier cible", 'default': ""},
        {'input': 'ftp_user', 'name': "Utilisateur FTP", 'default': ""},
        {'input': 'ftp_pass', 'name': "Mot de passe FTP", 'default': ""},
        {'input': 'ftp_port', 'name': "Port FTP", 'default': 21},
    ]


def get(id_environment):
    return cursor.execute("SELECT * FROM environments WHERE id=" + id_environment + ";").fetchone()


def get_environments():
    return cursor.execute("SELECT * FROM environments").fetchall()


def create(name, url, admin_url, ssh_user, ssh_pass, ssh_port, db_name, db_user, db_pass, db_url, db_port, folder, ftp_user, ftp_pass,
           ftp_port):
    cursor.execute(""" INSERT INTO environments(name, url, admin_url, ssh_user, ssh_pass, ssh_port, db_name, db_user, db_pass, db_url, 
    db_port, folder, ftp_user, ftp_pass, ftp_port) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)""",
                   (name, url, admin_url, ssh_user, ssh_pass, ssh_port, db_name, db_user, db_pass, db_url, db_port, folder, ftp_user,
                    ftp_pass, ftp_port))
    db.commit()


def delete(id_environment):
    cursor.execute("DELETE FROM environments WHERE id = " + id_environment)
    db.commit()


def update(id_environment, data):
    cursor.execute("""
                    UPDATE environments 
                    SET 
                        name = '""" + data[0] + """',
                        url = '""" + data[1] + """',
                        admin_url = '""" + data[2] + """',
                        ssh_user = '""" + data[3] + """',
                        ssh_pass = '""" + data[4] + """',
                        ssh_port = '""" + data[5] + """',
                        db_name = '""" + data[6] + """',
                        db_user = '""" + data[7] + """',
                        db_pass = '""" + data[8] + """',
                        db_url = '""" + data[9] + """',
                        db_port = """ + data[10] + """,
                        folder = '""" + data[11] + """',
                        ftp_user = '""" + data[12] + """',
                        ftp_pass = '""" + data[13] + """',
                        ftp_port = """ + data[14] + """
                    WHERE id = """ + id_environment + """ 
                   """)
    db.commit()
