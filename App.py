import tkinter as tk
import tkinter.ttk as ttk
import Environment
import Action
import sqlite3

with sqlite3.connect('data.db') as db:
    cursor = db.cursor()
    cursor.execute(""" CREATE TABLE IF NOT EXISTS environments(id integer PRIMARY KEY AUTOINCREMENT,
      name text NOT NULL, url text NOT NULL, ssh_user text, ssh_pass text, db_name text, db_user text, db_pass text,
      db_url text, db_port integer, folder text, ftp_user text, ftp_pass text, ftp_port integer) """)


class App:
    def __init__(self):
        self.main_window = tk.Tk()
        self.main_window.title("EMI")
        self.main_window.geometry("1200x720")
        self.main_window.minsize(480, 360)
        logo = tk.Image("photo", file="img/logo.png")
        self.main_window.tk.call('wm', 'iconphoto', self.main_window, logo)
        self.main_window.config(background='#313131')

        self.environments_frame = tk.Frame(self.main_window, bg='#545454', pady=10, padx=20)
        self.actions_frame = tk.Frame(self.main_window, bg='#313131', pady=10, padx=20)

        self.environments()
        self.actions()

        self.inputs = dict()
        self.input_labels = dict()

        self.main_window.mainloop()

    def environments(self):
        self.environments_frame.pack(side='left', fill='both', expand=True)

        title = tk.Label(self.environments_frame, text="Environnements", font=('Arial', 16), fg='#FFFFFF', bg='#545454')
        title.pack(side="top", fill='x')

        self.table = ttk.Treeview(self.environments_frame, columns=('id', 'environment', 'url', 'folder', 'db'))
        self.table.heading('id', text='#')
        self.table.column('id', width=30)
        self.table.heading('environment', text='Environnement')
        self.table.heading('url', text='URL')
        self.table.heading('folder', text='Dossier')
        self.table.heading('db', text='DB')
        self.table['show'] = 'headings'
        self.table.pack(padx=10, pady=(0, 10))

        environments = Environment.get_environments()
        for environment in environments:
            self.table.insert('', 'end', iid=environment[0],
                              values=(environment[0], environment[1], environment[2], environment[10], environment[5]))

    def create_environment_action(self, action):
        name = self.inputs['name'].get()
        url = self.inputs['url'].get()
        admin_url = self.inputs['admin_url'].get()
        ssh_user = self.inputs['ssh_user'].get()
        ssh_pass = self.inputs['ssh_pass'].get()
        ssh_port = self.inputs['ssh_port'].get()
        db_name = self.inputs['db_name'].get()
        db_user = self.inputs['db_user'].get()
        db_pass = self.inputs['db_pass'].get()
        db_url = self.inputs['db_url'].get()
        db_port = self.inputs['db_port'].get()
        folder = self.inputs['folder'].get()
        ftp_user = self.inputs['ftp_user'].get()
        ftp_pass = self.inputs['ftp_pass'].get()
        ftp_port = self.inputs['ftp_port'].get()

        if len(name) > 0 and len(url) > 0:
            if action == 'update':
                Environment.update(self.table.set(self.table.focus(), column='id'),
                                   [name, url, admin_url, ssh_user, ssh_pass, ssh_port, db_name, db_user, db_pass,
                                    db_url, db_port, folder,
                                    ftp_user, ftp_pass, ftp_port])
            else:
                Environment.create(name, url, admin_url, ssh_user, ssh_pass, ssh_port, db_name, db_user, db_pass,
                                   db_url, db_port, folder,
                                   ftp_user, ftp_pass, ftp_port)
            self.new_environment_window.destroy()
            self.refresh_environments()

    def refresh_environments(self):
        self.environments_frame.destroy()
        self.environments_frame = tk.Frame(self.main_window, bg='#545454', pady=10, padx=20)
        self.environments()

    def new_environment_action(self, action):
        self.new_environment_window = tk.Toplevel(self.main_window)
        if action == 'edit':
            self.new_environment_window.title('Modifier un environnement')
        else:
            self.new_environment_window.title('Nouvel environnement')
        self.new_environment_window.geometry("720x700")

        new_environment_frame = tk.Frame(self.new_environment_window, bg='#313131', pady=10, padx=20)

        fields = Environment.get_fields()
        i = 0

        for input_details in fields:
            self.input_labels[input_details['input']] = tk.Label(new_environment_frame, text=input_details['name'],
                                                                 font=('Arial', 16), fg='#FFFFFF', bg='#313131')
            self.input_labels[input_details['input']].grid(row=i, column=0, sticky='w', padx=10, pady=5)
            self.inputs[input_details['input']] = tk.Entry(new_environment_frame, width=40)
            if input_details['default'] != '' and action == 'create':
                self.inputs[input_details['input']].insert('end', input_details['default'])
            self.inputs[input_details['input']].grid(row=i, column=1, sticky='we', padx=10, pady=5)
            i += 1

        if action == 'edit':
            create_environment = tk.Button(new_environment_frame, text="Modifier l'environnement",
                                           font=("Courrier", 14), bg='white', fg='#313131',
                                           command=lambda: self.create_environment_action(action='update'))
        else:
            create_environment = tk.Button(new_environment_frame, text="Créer l'environnement", font=("Courrier", 14),
                                           bg='white', fg='#313131',
                                           command=lambda: self.create_environment_action(action='create'))
        create_environment.grid(row=99, column=1, sticky='we', padx=10, pady=(30, 5))

        if action == 'edit':
            current = Environment.get(self.table.set(self.table.focus(), column='id'))
            self.inputs['name'].insert('end', current['name'])
            self.inputs['url'].insert('end', current['url'])
            self.inputs['admin_url'].insert('end', current['admin_url'])
            self.inputs['ssh_user'].insert('end', current['ssh_user'])
            self.inputs['ssh_pass'].insert('end', current['ssh_pass'])
            self.inputs['ssh_port'].insert('end', current['ssh_port'])
            self.inputs['db_name'].insert('end', current['db_name'])
            self.inputs['db_user'].insert('end', current['db_user'])
            self.inputs['db_pass'].insert('end', current['db_pass'])
            self.inputs['db_url'].insert('end', current['db_url'])
            self.inputs['db_port'].insert('end', current['db_port'])
            self.inputs['folder'].insert('end', current['folder'])
            self.inputs['ftp_user'].insert('end', current['ftp_user'])
            self.inputs['ftp_pass'].insert('end', current['ftp_pass'])
            self.inputs['ftp_port'].insert('end', current['ftp_port'])

        new_environment_frame.pack(fill='both', expand=True)

    def actions(self):
        self.actions_frame.pack(side='right', fill='y')

        title = tk.Label(self.actions_frame, text="Actions", font=('Arial', 16), fg='#FFFFFF', bg='#313131')
        title.pack(side="top", fill='x')

        parameters = tk.Button(self.actions_frame, text="Paramètres", font=("Courrier", 14), bg='white', fg='#313131',
                               command=lambda: self.new_environment_action(action='edit'))
        parameters.pack(pady=10, fill='x')

        open_in_browser = tk.Button(self.actions_frame, text="Ouvrir dans le navigateur", font=("Courrier", 14),
                                    bg='white', fg='#313131', command=lambda: Action.open_in_browser(
                self.table.set(self.table.focus(), column='id')))
        open_in_browser.pack(pady=10, fill='x')

        open_admin_in_browser = tk.Button(self.actions_frame, text="Ouvrir la page d'administration", font=("Courrier", 14),
                                          bg='white', fg='#313131', command=lambda: Action.open_admin_in_browser(
                self.table.set(self.table.focus(), column='id')))
        open_admin_in_browser.pack(pady=10, fill='x')

        ssh = tk.Button(self.actions_frame, text="SSH", font=("Courrier", 14), bg='white', fg='#313131',
                        command=lambda: Action.ssh(self.table.set(self.table.focus(), column='id')))
        ssh.pack(pady=10, fill='x')

        dump = tk.Button(self.actions_frame, text="Dump DB", font=("Courrier", 14), bg='white', fg='#313131',
                         state='disabled')
        dump.pack(pady=10, fill='x')

        backup = tk.Button(self.actions_frame, text="Récupérer un backup", font=("Courrier", 14), bg='white',
                           fg='#313131', state='disabled')
        backup.pack(pady=10, fill='x')

        docker = tk.Button(self.actions_frame, text="Lancer docker", font=("Courrier", 14), bg='white', fg='#313131',
                           state='disabled')
        docker.pack(pady=10, fill='x')

        ftp = tk.Button(self.actions_frame, text="FTP", font=("Courrier", 14), bg='white', fg='#313131',
                        state='disabled')
        ftp.pack(pady=10, fill='x')

        new_environment = tk.Button(self.actions_frame, text="Nouvel environnement", font=("Courrier", 14), bg='white',
                                    fg='#313131', command=lambda: self.new_environment_action(action='create'))
        new_environment.pack(side='bottom', pady=10, fill='x')

        delete = tk.Button(self.actions_frame, text="Supprimer", font=("Courrier", 14), bg='white',
                           fg='#FF0000', command=self.delete_environment_action, state='disabled')
        delete.pack(side='bottom', pady=10, fill='x')

    def delete_environment_action(self):
        current = self.table.set(self.table.focus(), column='id')
        Environment.delete(current)
        self.refresh_environments()
